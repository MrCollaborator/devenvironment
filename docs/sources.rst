Sources
#######

The content of this guide is not only based on personal experience and my own setup. This collection is also influenced and inspired by other blog posts and articles on this topic. This is just a list of the articles and posts I can recommend on this issue. You will find many more ways and ideas of setting up your personal environment, so please have a look on your own.

Cisco Devnet
============

    Since my main career is being a Cisco collaboration solution engineer and consultant. And since I've written this guide primarily for a Devnet Express event session, this list of Leaning Labs from `Cisco Devnet Learning Labs`_ should not be omitted from this list.

    * `What is a Development Environment and why do you need one`_?

Others
======

    * `15 Ways to Bypass the PowerShell Execution Policy`_ by Scott Sutherland
    

.. _Cisco Devnet Learning Labs: https://developer.cisco.com
.. _What is a Development Environment and why do you need one: https://developer.cisco.com/learning/tracks/netdevops/dev-setup/dev-what/step/1
.. _15 Ways to Bypass the PowerShell Execution Policy: https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/