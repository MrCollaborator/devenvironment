Setup Guide - Template
######################

Overview
========

OS Requirements
===============

Project Directory
-----------------

Version Control - Git
=====================

[Language, e.g. Python]
=======================

(if available) Virtual Environment
----------------------------------

[Editor/IDE]
============

Download
--------

Installation
------------

Basic Setup
-----------

Integration with Git
--------------------

Syntax Highlighting
-------------------