Visual Studio Code
##################


Profile directories
-------------------
Settings are located at:

    Windows - %APPDATA%\Code\User\settings.json
    macOS   - $HOME/Library/Application Support/Code/User/settings.json
    Linux   - $HOME/.config/Code/User/settings.json

Extensions are located in the following locations:

    Windows - %USERPROFILE%\.vscode\extensions
    macOS   - ~/.vscode/extensions
    Linux   - ~/.vscode/extensions
