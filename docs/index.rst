Yet Another Development Environment Guide
#########################################

Docs Homepage: https://devenvironment.readthedocs.io/en/latest/

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   what_is_a_dev_env
   components
   setup
   sources

ToDo
====
* Git Proxy Information

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
