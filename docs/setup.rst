Setup
#####
Let's set up an environment for you. This section is divided into guides for different environment and deployments. And at the moment only one guide long. Others will hopefully follow, depending on my time and possible contributions (always welcome).

.. toctree::
   :maxdepth: 1
   :caption: Setup Guides:

   py_win10_vscode

Structure and Requirements
==========================
All of the guides in this section need to include a minimum set of requirements to meet a setup that can be used by developers:

* Operating System specific commands
* Git as version control
* Use of a virtual environment if available for the uses language(s)
* At least one Text Editor or IDE with at least:
  * Syntax Highlighting
  * Git Integration
* Tests and checks to see if the instructions worked

The basic chapter structure for each guide is:

- Overview
- OS Requirements
  - Project Directory
- Version Control - Git
- [Language, e.g. Python]
  - (if available) Virtual Environment
- [Editor/IDE]
  - Download
  - Installation
  - Basic Setup
  - Integration with Git
  - Syntax Highlighting

To write your own guide you can make a copy of the 'setupguide_template.rst' which includes the structure above.