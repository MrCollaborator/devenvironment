Python on Windows 10
####################


Overview
========
After successfully implementing this guide you should have a development environment for writing and running python applications directly through a Windows 10 terminal.

This setup includes:
- Version Control with Git
- Python 3.8+
- Virtual Environment for Python
- Microsoft Visual Studio Code as Editor


OS Requirements
===============
This setup is written for a Windows 10 platform.

Powershell Excecution-Policy
----------------------------
Windows 10 default security settings prevent you from running PowerShell scripts. In the "Virtual Environment" a PowerShell script is executed to activate the virtual environment. You might run into an error which points to the Excecution-Policy.

.. image:: _images/screen_ps_excecpolicy-error.png

You can check the current excecution policy by starting a PowerShell and running the command:

::

    Get-ExecutionPolicy

.. image:: _images/screen_ps_excecpolicy-get-restricted.png

There are several ways to overcome these restrictions. Quite a few of them are described in a blog article by Scott Sutherland which you can find under the :doc:`/sources` section.

For ease of use we will set the Policy to 'RemoteSigned'. This means that remotely executed PowerShell scripts need to be signed. Locally executed scripts don't need to meet this requirement. Set the policy and check the result afterwards.

::

    Set-ExecutionPolicy RemoteSigned
    Get-ExecutionPolicy

.. image:: _images/screen_ps_excecpolicy-set-remotesigned.png

.. warning:: Be aware that this allows the execution of local PowerShell scripts without them being signed.

Project Directory
=================
Set up a base directory for your development projects. It doesn't really matter if you have a structure below the base directory itself but it might help organizing certain things. 

My current structure is:

.. parsed-literal::

    #Development/
    ├──apps/
       ├── <-- contains my different applications, most projects are here -->
    ├──containers/
       ├── <-- repositories for my docker container repositories -->
    ├──docs/
       ├── <-- documentation projects like this one -->
    ├──snippets/
       ├── <-- small code snippets, single methods, ... -->


Version Control - Git
=====================
There are a few version control systems but we are using Git.

Download and Installation
-------------------------
Download the Windows Installer from: https://git-scm.com/downloads

Run the Installer as usual.

.. image:: _images/screen_git_install-01.png
   :align: center

After the installation is finished, an html page with notes and current issues for Git on Windows opens up. It's never a bad idea to read such notes.

You might have to reboot at this point since the git command shell will integrate into Path.

Initialize your first repository
--------------------------------
You can run git commands from both, CMD and PowerShell, or through Git Bash which comes with the installation. I'll use PowerShell for this section, you just use your terminal of choice.

We won't get into details of the usage of git but check a basic feature of it.

Change to your projects base directory and check the version of your git installation.

::

    git --version

You should see the current git version. If not, restart your machine to update your Path.

Git requires a basic configuration before you can push your first code to a remote repository so let's do this.

::

    git config --global user.name "Christian Drefke"

::

    git config --global user.email "christian.drefke@bechtle.com"

Create a directory, change into it and initialize your first local repository.

::

    mkdir myfirstrepo

::

    cd myfirstrepo

::

    git init

.. image:: _images/screen_git_cfg-dir-init.png
   :align: center

Git did now initialize a local repository in your directory. All necessary data for this repository is stored in the hidden directory '.git'.

::

    ls -Force


Python
======
Using Python on Windows 10 is quite straight forward nowadays.

Download and Installation
-------------------------
Download the Windows Installer from: https://www.python.org/downloads/

* Click on the 'Download Python' button
* Use the 'Windows x86-64 executable installer' for this setup

.. note::

    You can also use the embeddable zip file for installation but this requires more manual work than just using the installer.

Run the installer.

Make sure you have selected 'Add Python 3.8 to PATH'.

.. image:: _images/screen_py_install-main.png
   :align: center

If you choose 'Custom':

* Select 'pip'
* (optional) Select Documentation and/or tcl/tk 

.. image:: _images/screen_py_install-features.png
   :align: center

In the next screen, select your preferred installation directory and hit 'Install'. The installation might take a few minutes.

Run Python
----------
To check if the installation was successful, start a PowerShell and start a python interpreter.

::

    python

.. image:: _images/screen_py_install-features.png
   :align: center

When the interpreter started and you see a version number together with the interpreter prompt '>>>', your Python installation is ready to use.

.. note::

    You can leave the interpreter by typing in 'exit()' and Enter


Virtual Environment
-------------------

Why virtual environments?
-------------------------
Python is bringing quite a lot of useful modules with it but there are many projects out there that use external modules, usually installed from the `Python Packaging Index`_. But it might happen that you are using different versions of such module in different projects. And a newer version might have a certain method or class name changed in comparison to an earlier version. This might brake your applications and you would have to adapt these module updates in our code.

To avoid such dependency problems a pretty common practice is to use virtual environments. It is a good habit to initiate a venv right after creating the project directory. And after the virtual environment got set up, activating it each time before you start working on the project.

Installation
------------
Let's install virtualenv for python first. Start a PowerShell terminal and install virtualenv through the pip installer.

::

    pip install virtualenv

.. image:: _images/screen_py_install-virtualenv.png
   :align: center

You might get a notice that your pip version is out of date. This is no big issue but let's update pip at this point as well.

::

    python -m pip install --upgrade pip

Pip is now up to date.

Initialize and activate a Virtual Environment
---------------------------------------------
To initialize a virtual environment, switch the directory that we created earlier. From our base development directory:

::

    cd myfirstrepo

Now initialize the venv. We will use '.venv' as the name for the environment data directory.

::

    virtualenv .venv

.. image:: _images/screen_py_init-virtualenv.png
   :align: center

We now have an additional directory in our project which contains a copy of our python interpreter including it's scripts like pip. But to make use of this new venv we need to activate it first.

::

    .venv/Scripts/activate.ps1

You should now see a prefix '(.venv)' in front of your command prompt. This tells us that we have successfully activated the virtual environment.

.. note::

    Actually there's an 'deactivate.bat' under the venv 'Scripts' directory as well but at least in my case this has never really worked. Simply close the terminal session or use the 'activate.ps1' in a different project folder to switch to it.

.. note::

    Use the 'activate.bat' file if you are using CMD as your terminal.

Visual Studio Code
==================

Download
--------
Download the Windows Installer from: https://code.visualstudio.com/


Installation
------------
The installation is basically straight forward but you need to lookout for the PATH variable. Check the box so that VSCode is included.

.. image:: _images/screen_vscode_install-path.png
   :align: center

After installation start the program to see if it's running.

.. image:: _images/screen_vscode_welcome.png
   :align: center

Basic Setup
-----------
Every code editor comes with a sophisticated set of features but don't necessarily have support for syntax highlighting, remote server access (FTP, SFTP, ...) already included. 

We'll make a very basic setup of VSCode for Python and Git.

Syntax Highlighting
-------------------
Not having prober syntax highlighting when writing code is a major pain and definitely not fun. Especially syntax errors will happen way more often than with proper highlighting.

To enable highlighting for the Python syntax all we need to do is to enable the extension for it. On the left side of the VSCode window select Extensions (or Ctrl+Shift+X).

.. image:: _images/screen_vscode_extensions.png
   :align: center

You should see Python right away. Otherwise use the search field. Click "Install" to install the extension.

.. image:: _images/screen_vscode_install-python.png
   :align: center

Nothing is better for checking syntax highlighting than writing a few lines of code. To do so, open your projects directory that we created earlier. File -> Open or Ctrl+K, Ctrl+O.

.. image:: _images/screen_vscode_file-open.png
   :align: center

After opening the directory you should see it on the left side in the Explorer section (else Ctrl+Shift+E to open). We'll now create a new file (Ctrl+N).

.. image:: _images/screen_vscode_file-new.png
   :align: center

And add some content to it.

.. code::

    answer = input("The answer to the ultimate question of life, the universe and everything is ")

    print(answer)

Now save (Ctrl+S) your file and give it a proper filename with file extension ".py" which is the typical file extension for Python files.

If you did a fresh installation of VSCode you will probably see the following info message at the lower right corner.

.. image:: _images/screen_vscode_mgs-pylint.png
   :align: center

A linter, here specifically pyLint are extensions or modules that check your written code against syntax and code quality rules. If you like to, install this extension. You can skip this for now but you can't start too early to write well formatted code.

Back to our repository you should see the new file in your explorer section.

.. image:: _images/screen_vscode_explorer-dougadams.png
   :align: center

VSCode gives you the possibility to directly run you newly written script.

.. image:: _images/screen_vscode_run-script.png
   :align: center

You'll see the script output in the terminal section of VSCode. And in this case the script even expects an input from your side. So provide an answer to finish the script.

.. image:: _images/screen_vscode_terminal-answer.png
   :align: center

Èt voila, if we did everything right you just ran your first Python script.

Integration with Git
--------------------
VSCode already has an integration for Git and should recognize the hidden '.git' directory from our repository automagically.

To check our repository, change to the version control section (Ctrl+Shift+G).

In this section we see all files that git recognizes as changed.

You should see plenty of changed files at this stage cause by our virtual environment directory '.venv'. But since we definitely don't want to have these files in our repository, we need to make sure that Git won't include them. This is where we create a new file in our directory called '.gitignore'.

.. image:: _images/screen_vscode_git-ignore.png
   :align: center

And add a single line to it.

.. code::

    # Environments
    .venv

    # Settings
    .vscode

Git will ignore everything that is defined in such a .gitignore file. In our case the whole directory '.venv'. Save the file when ready. You should now see way less files than before. If not then something went wrong here.

Before we can commit our files to our repository, we need to stage them. This is part of a typical Git workflow and topic at some other time.

.. image:: _images/screen_vscode_git-stageall.png
   :align: center

After all files are staged, write a commit message (mandatory) and commit (Ctrl+Enter).

.. image:: _images/screen_vscode_git-firstcommit.png
   :align: center

I guess that's it for now. We have set up a small development environment on Windows 10 and you should be able to write and run your first applications without breaking them by dependency issues.

What next?
==========
Well, there are still plenty of things you can do to optimize your environment. But also many things to learn for becoming more familiar with your tools and your personal workflow.

Some homework for the near future:

* Get to know Git a little bit better - Atlassian has a pretty nice `manual`_ for this
* Learn Python - 
* 

.. _Python Packaging Index: https://pypi.org/
.. _manual: 