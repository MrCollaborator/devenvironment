Components
##########
Components of a development environment

Operating System
================
Not going into details here, we need at least some kind or operating system to run our environment on.

Linux
-----
Probably the most powerful OS for a developer but also the one that many beginners struggle with. At least when they are used to Windows environments before. But give it a try, though it might not be your first choice on the client side it will definitely be the OS that you will see most on the server side.

Windows
-------
Developing on Windows was quite a pain with earlier versions of the OS. At least as long as you didn't develop for the Windows ecosystem working with languages like .NET or VBScript.
Especially developing with interpreter languages like Python or Perl had quite a few drawbacks. I personally had at least one linux virtual machine available to get around the issues with windows based developing.

But these times are over since we have Windows 10. It made a large step towards a developer friendly environment. There are still a few gaps and hickups (especially when running Docker containers locally) but the requirements are satisfied.

See this article from `Alexander Lockshyn`_ as example for someone who has the same feeling: `Surprisingly Software Development on Windows is Awesome Now`_

Mac OS
------
Can't really say that much from a personal point of view about Mac OS but I know that it's great for developers. Plenty out there are using it for a long time. And it's relationship and common features to Unix/Linux are probably the main reason for this success. It might be the perfect OS for having both, a robust development environment and a well designed UI operating system.

Terminals and Shells
====================
I always prefer a good CLI over a good UI and sometimes even a great UI. There are plenty out there and they can be used more and more universally between the different operating systems:

* `Bourne shell`_ - Good old 'bash' from Unix/Linux based systems. Still going strong.
* `Z shell`_ - This extended bash, shortly called 'zsh', is used as the default shell on Macs since macOS Catalina.
* `PowerShell`_ - Open Source since 2016, this shell from Microsoft can be installed not only on Windows but also Unix/Linux based systems. Powershell 7, stable since March 4, 2020, is the universal replacement for older 5.x and 6.x versions.

Even if you prefer to use your mouse for all operations, you'll need to run some commands here and there to start your scripts, applications, containers, ... through a terminal.

Programming Languages
=====================
Programming languages are divided in two major families, compiler and interpreter languages.

Compiler Languages
------------------
    An application written in a compiler language needs to be translated by a compiler into "machine code"/"assembly" before it can be executed on a computer system. The compiler is only needed for the translation, the compiled application can run standalone afterwards.

    Examples: C++, Go, Java

Interpreter Languages
---------------------
    Code written in an interpreted language needs an interpreter to be executed. The interpreter is processing the code "on the fly".

    Examples: `Perl`_, `Python`_, `Ruby on Rails`_

Editors
=======

Text Editors
------------
    A text editor is, well, a text editor. There are plenty out there and in many cases a text editor can be upgraded through add-ons to support developing features like syntax highlighting, code completion or even version control integration.

    Examples: `Atom`_, `Notepad++`_, `Sublime`_, `Visual Studio Code`_


Integrated Development Environment
----------------------------------
An "Integrated Development Environment" or short IDE is a "batteries included" software that helps you to write, test and deliver your source code and applications in a specific programming language. Main differentiations between an IDE and even sophisticated text editors like Notepad++ are:

* Integration in version control systems like Git
* Dependency checking
* Running your application through an interpreter for debugging directly in your IDE window
* Visual programming

And of course the features that a good text editor can do too like syntax highlighting or code search.


.. _Alexander Lockshyn: http://mourk.com/
.. _Surprisingly Software Development on Windows is Awesome Now: https://medium.com/short-stories-on-software/surprisingly-software-development-on-windows-is-awesome-now-82251d66991a
.. _Bourne shell: https://en.wikipedia.org/wiki/Bourne_shell
.. _Z shell: https://en.wikipedia.org/wiki/Z_shell
.. _Powershell: https://en.wikipedia.org/wiki/PowerShell
.. _Perl: https://www.perl.org/
.. _Python: https://www.python.org/
.. _Ruby on Rails: https://rubyonrails.org/
.. _Atom: https://atom.io/
.. _Notepad++: https://notepad-plus-plus.org/
.. _Sublime: https://www.sublimetext.com/
.. _Visual Studio Code: https://code.visualstudio.com/