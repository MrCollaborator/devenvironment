What is a Development Environment?
##################################

Basics
======
Think of a development environment like the workbench of a handyman or the setup of an operation room with all it's instruments and tools. Each tool has it's function or use and it's fixed place. But different kinds of operations or crafting projects might need a different toolset. And every handyman or surgeon might even use their own preferred tools in their own preferred way.

Other professions that come in mind when thinking about such environments: Graphic Designers, Financial Traders, Emergency Responders, Software Developers.

The environment of a software developer also needs to be structured and set up well to be able to jump right into an existing or new project without struggling to get up to speed. It's necessary to have a collection of tools and methods, working together with your operating system in both, development and production stage.

Environment Location
====================
An environment can run local on your notebook or workstation, in a virtual machine or even remote.

Local Development Environment
-----------------------------
    The first option for the development environment is to install and configure the development software and tools directly on your local workstation. This provides a high degree of customization and personalization for the developer to choose exactly how they want to work, and because it's all installed locally, it provides a highly portable option.

Hosted Development Environments
-------------------------------
    In the hosted development environment option, rather than installing the software and tools directly on your local workstation, the developer leverages a virtual machine (shared or dedicated) from a hosting provider where the tools are installed and made available. Developers can then use remote access technologies such as remote desktop or SSH to connect to the hosted environment to do their development.

Cloud Development Environments
------------------------------
    A cloud development environment is a type of hosted environment where developers will often access their tools from within a web application.

*Definitions from:* `What is a Development Environment and why do you need one`_

Tiers - Stages of development
=============================
There are usually three tiers when developing an application. In a perfect world every developer is running new code through all of these tiers, manually or automatically. When talking about running the code, it basically means to run it against the server side or backend. Therefore we talk about tiers on the server side.

Development Tier
----------------
    All development is done in the development tier. The developer is running and testing the application against a backend that is not running and production critical processes. Depending on the project this tier needs a whole setup of server backends, hardware and other things to test the application.
    In many cases additional tools or scripts or logging features are used to debug code more efficiently during development. These additions are not necessary or purposely deactivated during production.

Staging Tier
------------
    A staging tier is basically a copy of the production tier. The application is tested against this environment. Long running performance and stability tests are usually run against this. If something is wrong with the application, the developer is going back to the development tier to fix identified issues.

Production Tier
---------------
    If all tests are ok and the application is cleared for production, this is where it's activated and running. There should be no development, bugfixing whatsover to this tier before being tested through the other tiers.


.. _What is a Development Environment and why do you need one: https://developer.cisco.com/learning/tracks/netdevops/dev-setup/dev-what/step/1